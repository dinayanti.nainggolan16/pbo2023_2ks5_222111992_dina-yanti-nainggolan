package com.DINA.RESTAPI.restapi.controller;

import com.DINA.RESTAPI.restapi.model.Student;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping
public class StudentController
{
    private List<Student> studentList = new ArrayList<>();

    // Metode untuk menambahkan siswa baru
    @PostMapping("/students")
    public String addStudent(@RequestBody Student student) {
        studentList.add(student);
        return "Student Created Succesfully";
    }

    // Metode untuk mengupdate informasi siswa
    @PutMapping("/students/{id}")
    public String updateStudent(@PathVariable("id") int id, @RequestBody Student student) {
        Student existingStudent = studentList.stream()
                .filter(s -> s.getId() == id)
                .findFirst()
                .orElse(null);
        if (existingStudent != null) {
            existingStudent.setName(student.getName());
            existingStudent.setClassName(student.getClassName());
            existingStudent.setAddress(student.getAddress());
        }
        return "Student Updated Succesfully";
    }
    // Metode untuk menghapus siswa
    @DeleteMapping("/students/{id}")
    public String deleteStudent(@PathVariable("id") int id) {
        Student existingStudent = studentList.stream()
                .filter(s -> s.getId() == id)
                .findFirst()
                .orElse(null);
        if (existingStudent != null) {
            studentList.remove(existingStudent);
        }
        return "Student Deleted Succesfully";
    }

    // Metode untuk mendapatkan daftar siswa
    @GetMapping("/students")
    public List<Student> listStudent() {
        return studentList;
    }
}
