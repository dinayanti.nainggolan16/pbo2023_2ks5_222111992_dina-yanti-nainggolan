package Modul5;
/**
 *
 * @author DINA NAINGGOLAN
 */
interface Dosen {
    String NIDN = "";
    String kelompokKeahlian = "";
    
    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKeahlian();
    public String getPekerjaan();
    public void setKeahlian(String Keahlian);
}
