package Modul5;
import java.time.LocalDate;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Test {
    public static void main(String[] args) {
    
    Dosen STIS = new Pegawai("Dina",LocalDate.of(2002,10, 04));
    Pegawai Wendy = (Pegawai) STIS;
    Programmer Yanti = new Programmer("Yanti",LocalDate.of(2001,9, 04));
    
    Wendy.setNIDN("222111333");
    Wendy.setNama("Wendy Steven");
    Wendy.setKeahlian("Pemrogramman");
    System.out.println("Ada dosen " + Wendy.getNama() + " " + Wendy.getNamaPanggilan() + " dengan NIDN " + Wendy.getNIDN() +
                " kelompok " + Wendy.getKeahlian() + " dan Pekerjaan " + Wendy.getPekerjaan());
    
    Yanti.setNama("Dina Yanti");
    Yanti.setBahasa("Python");
    Yanti.setPlatform("Windows");
    System.out.println("Ada Programmer dengan nama " + Yanti.getNama() + " Bahasa Pemrograman " + Yanti.getBahasa() +
                " Platform " + Yanti.getPlatform() + " dengann Moto " + Yanti.getPekerjaan());
    }
 }
