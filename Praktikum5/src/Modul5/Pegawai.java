package Modul5;
import java.time.LocalDate;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Pegawai extends Orang implements Dosen {
    private String NIP;
    private String Kantor;
    private String unitKerja;
    String NIDN = "";
    String keahlian = "";

    public Pegawai(String nama, LocalDate tanggalLahir) {
        super(nama, tanggalLahir);
    }

    public String getNIP(){
        return NIP;
    }
    
    public String getKantor(){
        return Kantor;
    }
    
    public void setNIP(String NIP){
        this.NIP = NIP;
    }
    
    public void setKantor (String Kantro){
        this.Kantor = Kantor;
    }
    
    public void getUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }

    public String getUnitKerja() {
        return unitKerja;
    }

    public void setUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }
    
    @Override
    public String getNIDN() {
        return NIDN;
    }
    @Override
    public void setNIDN(String NIDN) {
        this.NIDN = NIDN;
    }

    @Override
    public String getKeahlian() {
        return keahlian;
    }

    @Override
    public void setKeahlian(String Keahlian) {
        this.keahlian = Keahlian;
    }

    @Override
    public String getPekerjaan() {
        return "mengajar";
    }

}
