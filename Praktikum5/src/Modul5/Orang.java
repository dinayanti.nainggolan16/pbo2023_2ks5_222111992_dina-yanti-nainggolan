package Modul5;
import java.time.LocalDate;
/**
 *
 * @author DINA NAINGGOLAN
 */
public abstract class Orang {
    private String nama;
    private LocalDate tanggalLahir;
    
    public Orang(String nama, LocalDate tanggalLahir){
      this.nama = nama;
      this.tanggalLahir = tanggalLahir;
    }
    
    public void setNama(String nama){
      this.nama = nama;
    }
    public String getNama(){
    return nama;
    }
    public String getNamaPanggilan(){
    return String.format("Nama Panggilan %.4s", nama);
    }
}

