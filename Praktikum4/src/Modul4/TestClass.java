package Modul4;
import java.util.Date;
/**
 *
 * @author DINA NAINGGOLAN
 */
public class TestClass {
    public static void main(String[] args) {
    
 //     Orang o = new Orang(); --> error karena Orang merupakan abstract
        
        //Tidak Mengirimkan Parameter Karena ingin mecoba set parameter sendiri
        Pegawai lutfi = new Pegawai();
        lutfi.setAlamat("Otista 64 C");
        lutfi.setNIDN("12345678");
        lutfi.setKelompokKeahlian("Computer Science");
        System.out.println("Ada dosen Lutfi dengan NIDN " +lutfi.getNIDN()+" kelompok "+lutfi.getKelompokKeahlian()+"\nTinggal di "+lutfi.getAlamat());
        lutfi.setNamaKantor("STIS");
        lutfi.setUnitKerja("Back-end Developer");
        System.out.println("Bekerja di "+lutfi.getNamaKantor()+" pada bagian "+lutfi.getUnitKerja());
    
        //Test Kelas Programmer
        System.out.println("");
        Programmer Dina = new Programmer("Dina", new Date(2002, 10, 04), "222111992", "STIS", "IT Dev", "Java", "NetBeans");
        System.out.println("Deskripsi Programmer Dina: ");
        System.out.println("Bahasa Pemrograman: "+Dina.getBahasaPemrograman());
        System.out.println("Platform yang digunakan: "+Dina.getPlatform());
        
    }
}
