package Modul4;
import java.util.Date;
/**
 * author DINA NAINGGOLAN
 */
public class Programmer extends Pegawai{
    private String bahasaPemrograman;
    private String platform;
    
    public Programmer(String nama, Date tanggalLahir, String NIP, String kantor, String unitKerja, String bp, String platform) {
        super(nama, tanggalLahir, NIP, kantor, unitKerja);
        this.bahasaPemrograman = bp;
        this.platform = platform;
    }
    
    public void setBahasaaPemrograman(String bp){
        this.bahasaPemrograman = bp;
    }
    
    public String getBahasaPemrograman(){
        return bahasaPemrograman;
    }
    
    public void setPlatform(String platform){
        this.platform = platform;
    }
    
    public String getPlatform(){
        return platform;
    }
}
