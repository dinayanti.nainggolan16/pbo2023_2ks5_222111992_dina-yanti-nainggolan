package Modul4;
import java.util.Date;
/**
 * author DINA NAINGGOLAN
 */
public abstract class Orang {
    private String nama;
    private Date tanggalLahir;
    
    public Orang(){
    }
    
    public Orang(String nama){
        this.nama = nama;
    }
    
    public Orang(String nama, Date tanggalLahir){
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }
    
    abstract public String getAlamat();
    abstract public void setAlamat(String alamat);   
}