package Modul4;

/**
 * author DINA NAINGGOLAN
 * Deskripsi kelas Dosen :interface bertipe public agar dapat diakses di kelas berbeda

 */
interface Dosen {
    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKelompokKeahlian();
    public void setKelompokKeahlian(String kelompokKeahlian);
}
