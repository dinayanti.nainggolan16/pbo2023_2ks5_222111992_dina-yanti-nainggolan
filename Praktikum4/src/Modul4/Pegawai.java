package Modul4;
import java.util.Date;  
/**
 * author DINA NAINGGOLAN
 * Deskripi kelas Pegawa :tambahan implements dari interface dosen

 */
public class Pegawai extends Orang implements Dosen{
private String NIP;
    private String namaKantor;
    private String unitKerja;
    private String alamat;
    // untuk data Dosen nanti
    String NIDN;
    String keahlian;
    
    public Pegawai(){
    }
    
    public Pegawai(String NIP, String namaKantor, String unitKerja){
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    public Pegawai(String nama, Date tanggalLahir, String NIP, String namaKantor, String unitKerja){
        super(nama, tanggalLahir);
        this.NIP = NIP;
        this.namaKantor = namaKantor;
        this.unitKerja = unitKerja;
    }
    
    @Override
    public String getAlamat(){
        return alamat;
    }
    
    @Override
    public void setAlamat(String alamat) {
       this.alamat = alamat;
    }
    
    public String getNIP(){
        return NIP;
    }
    
    public void setNIP(String NIP){
        this.NIP = NIP;
    }
    
    public String getNamaKantor(){
        return this.namaKantor;
    }
    
    public void setNamaKantor(String namaKantor){
        this.namaKantor = namaKantor;
    }

    public String getUnitKerja(){
        return unitKerja;
    }
    
    public void setUnitKerja(String unitKerja){
        this.unitKerja = unitKerja;
    }
    
    
    public String getGaji(){
        return "7 Juta";
    }

//Pada baris ini ke bawah digunakan untuk mengakses interface dosen
    @Override
    public String getNIDN() {
        return NIDN;
    }
    
    @Override
    public void setNIDN(String NIDN) {
        this.NIDN = NIDN;
    }

    @Override
    public String getKelompokKeahlian() {
        return keahlian;
    }

    public void setKelompokKeahlian(String kelompokKeahlian) {
        this.keahlian = kelompokKeahlian;
    }
}
