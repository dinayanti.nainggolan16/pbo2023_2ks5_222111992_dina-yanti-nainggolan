/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul3;
import java.util.List;
/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :Unit Kerja
*/
public class UnitKerja {
    private String nama;
    private List <Pegawai> daftarPegawai;
    
    public UnitKerja(String nama, List <Pegawai> pegawais){
        this.nama = nama;
        this.daftarPegawai = pegawais;
    }
    /**
     * @return the nama
     */
    public String getNama(){
            return nama;
    }
    /**
     * @return the daftarPegawai
     */
    public List <Pegawai> getDaftarPegawai(){
        return daftarPegawai;
    }
}
