/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul3;

/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :Ruang
*/
public class Ruang {
    private String namaRuang;
    
    public Ruang(String namaRuang){
        this.namaRuang = namaRuang;
    }
    
    /**
     * @return the namaRuang
     */
    public String getNamaRuang(){
        return namaRuang;
    }
    
    /**
     * @param namaRuang the namaRuang to set
     */
    public void setNamaRuang(String namaRuang){
        this.namaRuang = namaRuang;
    }
}
