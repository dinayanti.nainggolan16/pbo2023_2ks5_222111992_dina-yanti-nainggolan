/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul3;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;

/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :kantor
*/
public class Kantor {
    /**
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Orang lutfi = new Orang();
        lutfi.setNama("Lutfi");
        
        Orang rahma = new Orang ("Rahma");
        rahma.setTanggalLahir(new Date(1997, 8, 31));
        
        System.out.println("Ada orang:");
        System.out.println(lutfi.getNama()+ "lahir pada" + lutfi.getTanggalLahir());
        System.out.println(rahma.getNama()+ "lahir pada" + rahma.getTanggalLahir());
        
        Pegawai tuti = new Pegawai ("Tuti", new Date(1997, 8, 2), "6836492379", "STIS", "IT");
        System.out.println(tuti.getNama()+ "lahir pada" + tuti.getTanggalLahir()+ "NIP:" + tuti.getNIP()+ "kantor:" + tuti.getNamaKantor()+ "bagian:"+ tuti.getUnitKerja());
        
        System.out.println("gaji Orang Rahma:" + rahma.getGaji());
        System.out.println("gaji Orang Tuti:" + tuti.getGaji());
    
        List <Pegawai> daftarPegawai = new ArrayList<Pegawai>();
        daftarPegawai.add(tuti);
        UnitKerja Umum = new UnitKerja("Umum", daftarPegawai);
        for(Pegawai staff: daftarPegawai){
            System.out.println("nama: " + staff.getNama());
        }
        
        Gedung STIS = new Gedung();
        STIS.addRuang("Lobi");
        STIS.addRuang("Bagian");
        
        List <Ruang> ruangan = STIS.getDaftarRuang();
        for(Ruang ruang: ruangan)
                System.out.println("Ruang: "+ ruang.getNamaRuang());
        }
    }

