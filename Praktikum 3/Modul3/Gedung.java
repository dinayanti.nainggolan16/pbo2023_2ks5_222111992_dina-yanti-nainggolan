/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul3;
import java.util.List;
import java.util.ArrayList;
/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :Ruang
*/
public class Gedung {
    private List<Ruang> daftarRuang = new ArrayList <Ruang>();
    
    public Gedung(){
        Ruang ruang = new Ruang("Utama");
        daftarRuang.add(ruang);
    }
    
    public void addRuang(String namaRuang){
        Ruang ruang = new Ruang(namaRuang);
        getDaftarRuang().add(ruang);
    }
    
    /**
     * @return the daftarRuang
     */
    public List<Ruang> getDaftarRuang(){
        return daftarRuang;
    }
}
