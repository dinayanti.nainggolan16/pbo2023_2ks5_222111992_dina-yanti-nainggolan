package UAS_PBO_2KS5_Dina_222111992.dto;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Email {
    private String to;
    private String subject;
    private String pesan;
    
    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPesan() {
        return pesan;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
    
    @Override
    public String toString(){
        return "Email[to=" + to + ", subject=" + subject + ", pesan=" + pesan + "]";
    }
    
}
