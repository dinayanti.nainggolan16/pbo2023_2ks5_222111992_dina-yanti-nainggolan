package UAS_PBO_2KS5_Dina_222111992.dto;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Builder; // Builder Pattern
import lombok.Data;
import lombok.NoArgsConstructor;
/**
 * @author Dina
 * 222111992
 * 2KS5
 * Representasi Model di Presentation Layer
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    private Long id;
    @NotEmpty(message = "Name should not be empty")
    private String name;
    @NotEmpty(message = "NIM should not be empty")
    private String nim;
    private int jan;
    private int feb;
    private int mar;
    private int apr;
    private int mei;
    private int juni;
    private int kurang;
    private String ket;      
}
