package UAS_PBO_2KS5_Dina_222111992.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import UAS_PBO_2KS5_Dina_222111992.entity.User;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
/**
 *
 * @author Dina 
 * 222111992
 * 2KS5
 */

public interface UserRepository extends JpaRepository<User, Long> {

    User findByEmail(String email);
}
