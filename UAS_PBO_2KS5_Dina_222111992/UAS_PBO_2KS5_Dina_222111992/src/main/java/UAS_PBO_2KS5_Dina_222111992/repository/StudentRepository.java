package UAS_PBO_2KS5_Dina_222111992.repository;

import UAS_PBO_2KS5_Dina_222111992.entity.Student;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 * 
 * extends interface JpaRepository untuk mengabstraski CRUD dan fungsi lainnya
 * Opsional : Baru di Java 8, Optional is generally used as a return type 
 *            for methods that might not always have a result to return (empty).
 * 
 * Bisa ditambahkan method2x abstract baru disini yang berkaitan dengan masalah repository dan tidak 
 * tercakup pada interface Repository induk-induknya. 
 * 
 * Data Access Layer
 * 
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
    // contoh method abstract baru. 
    //Optional<Student> findByNIM(String nim);
    
    // tambahkan method abstract lain disini yg bisa digunakan oleh service class jika diperlukan.
    //@Query("SELECT s from Student s WHERE " +
    //        " s.Name LIKE CONCAT('%', :query, '%') OR " +
    //        " s.NIM LIKE CONCAT('%', :query, '%')")
    //List<Student> searchStudent(String query);
    //Menggunakan 2 parameter yang dikirim, yaitu firstName atau lastName namun akhirnya keywordnya tetep sama
    List<Student> findByNameContainingOrKetContaining(String keyword, String keywords);
    
    //pagination
    Page<Student> findByKetContainingIgnoreCase(String keyword, Pageable pageable);
    Page<Student> findByNameContainingIgnoreCase(String keyword, Pageable pageable);
    Page<Student> findAll(Pageable pageable);
}
