package UAS_PBO_2KS5_Dina_222111992.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import UAS_PBO_2KS5_Dina_222111992.entity.Role;
/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 */

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}

