package UAS_PBO_2KS5_Dina_222111992.repository;

import UAS_PBO_2KS5_Dina_222111992.entity.Note;
import java.util.Optional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
/**
 *
 * @author DINA NAINGGOLAN
 */

public interface NoteRepository extends JpaRepository<Note, Long>{
    //Note findByJudul(String Judul);
    // tambahkan method abstract lain disini yg bisa digunakan oleh service class jika diperlukan.
    //@Query("SELECT n from Note n WHERE " +
    //        " n.Judul LIKE CONCAT('%', :query, '%') OR " +
     //       " n.Isi LIKE CONCAT('%', :query, '%')")
    //List<Note> searchNote(String query);
}
