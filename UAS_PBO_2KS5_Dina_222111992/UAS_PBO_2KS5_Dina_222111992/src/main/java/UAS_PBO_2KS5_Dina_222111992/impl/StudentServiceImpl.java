package UAS_PBO_2KS5_Dina_222111992.impl;

import UAS_PBO_2KS5_Dina_222111992.dto.StudentDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Student;
import UAS_PBO_2KS5_Dina_222111992.mapper.StudentMapper;
import UAS_PBO_2KS5_Dina_222111992.repository.StudentRepository;
import UAS_PBO_2KS5_Dina_222111992.service.StudentService;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 * 
 * implementasi class dari interface StudentService 
 * menggunakan StudentRepository 
 * dan StudentMapper (tiap objek data yang berasal dari dan digunakan 
 * di layer Presentation wajib menggunakan Tipe StudentDto)
 * 
 * Business Logic Layer
 */
@Service
public class StudentServiceImpl implements StudentService {    
    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }
    
    @Override
    public List<StudentDto> ambilDaftarStudent() {
        List<Student> students = this.studentRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi map()
        List<StudentDto> studentDtos = students.stream()
                .map((student) -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());        
        return studentDtos;
    }
    
    @Override
    public void hapusDataStudent(Long studentId) {
        studentRepository.deleteById(studentId);        
    }
    
    @Override
    public void perbaruiDataStudent(StudentDto studentDto) {
        Student student = StudentMapper.mapToStudent(studentDto);
        System.out.println(studentDto);
        studentRepository.save(student);
    }
    
    @Override
    public void simpanDataStudent(StudentDto studentDto) {
      Student student = StudentMapper.mapToStudent(studentDto);
        //System.out.println(student);
        studentRepository.save(student);
    }

    @Override
    public StudentDto cariStudentById(Long id) {
        Student std = studentRepository.findById(id).get();
        return  StudentMapper.mapToStudentDto(std);
    }
    
    
    //Methode untuk melakukan pencarian search
    @Override
    public List<StudentDto> searchStudent(String keyword) {
        //dilakukan dengan mengirim 2 parameter dari firstName dan lastName
        List<Student> students = this.studentRepository
        .findByNameContainingOrKetContaining(keyword, keyword);
        List<StudentDto> studentDto = students.stream()
            .map((student) -> (StudentMapper.mapToStudentDto(student)))
            .collect(Collectors.toList());
        return studentDto;
    }

    @Override
    public Page<Student> findByKetContainingIgnoreCase(String keyword, Pageable pageable) {
          return studentRepository.findByKetContainingIgnoreCase(keyword, pageable); 
    }
    
    @Override
    public Page<Student> findByNameContainingIgnoreCase(String keyword, Pageable pageable) {
        return studentRepository.findByNameContainingIgnoreCase(keyword, pageable);
    }
    
    @Override
    public Page<Student> findAll(Pageable pageable) {
      return studentRepository.findAll(pageable);
    }

    

}
