package UAS_PBO_2KS5_Dina_222111992.impl;

import UAS_PBO_2KS5_Dina_222111992.dto.Email;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

/**
 *
 * @author DINA NAINGGOLAN
 */
@Service
public class EmailService {
   
   @Autowired 
   private JavaMailSender javaMailSender;
   
   public void sendMail(Email em){
       try{
           MimeMessage pesan=javaMailSender.createMimeMessage();
           MimeMessageHelper helper=new MimeMessageHelper(pesan);
           helper.setFrom("lookstiser@gmail.com");
           helper.setTo(em.getTo());
           helper.setSubject(em.getSubject());
           helper.setText(em.getPesan());
           
           javaMailSender.send(pesan);
           
       }catch (Exception e){
           e.printStackTrace();
       }
   }
}
