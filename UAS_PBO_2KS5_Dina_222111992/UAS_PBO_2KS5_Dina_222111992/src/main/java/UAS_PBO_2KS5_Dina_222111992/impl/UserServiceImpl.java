package UAS_PBO_2KS5_Dina_222111992.impl;

import UAS_PBO_2KS5_Dina_222111992.dto.UserDto;
import UAS_PBO_2KS5_Dina_222111992.entity.User;
import UAS_PBO_2KS5_Dina_222111992.entity.Role;
import UAS_PBO_2KS5_Dina_222111992.service.UserService;
import UAS_PBO_2KS5_Dina_222111992.repository.UserRepository;
import UAS_PBO_2KS5_Dina_222111992.repository.RoleRepository;
import UAS_PBO_2KS5_Dina_222111992.mapper.UserMapper;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Arrays;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 */
@Service
public class UserServiceImpl implements UserService {
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository,
                           RoleRepository roleRepository,
                           PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void saveUser(UserDto userDto) {
        User user = UserMapper.mapToUser(userDto);
        // encrypt the password using spring security
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        Role role = roleRepository.findByName("ROLE_USER");
        if(role == null){
            role = checkRoleExist();
        }
        user.setRoles(Arrays.asList(role));
        userRepository.save(user);
    }

    @Override
    public User findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<UserDto> findAllUsers() {
        List<User> users = userRepository.findAll();
        return users.stream()
                .map((user) -> UserMapper.mapToUserDto(user))
                .collect(Collectors.toList());
    }


    private Role checkRoleExist(){
        Role role = new Role();
        role.setName("ROLE_USER");
        return roleRepository.save(role);
    }


}