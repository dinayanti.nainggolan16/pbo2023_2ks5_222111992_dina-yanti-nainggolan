package UAS_PBO_2KS5_Dina_222111992.impl;

import UAS_PBO_2KS5_Dina_222111992.dto.NoteDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Note;
import UAS_PBO_2KS5_Dina_222111992.mapper.NoteMapper;
import UAS_PBO_2KS5_Dina_222111992.repository.NoteRepository;

import UAS_PBO_2KS5_Dina_222111992.service.NoteService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;
/**
 *
 * @author DINA NAINGGOLAN
 */
@Service
public class NoteServiceImpl implements NoteService {
    private NoteRepository noteRepository;

    public NoteServiceImpl(NoteRepository noteRepository) {
        this.noteRepository = noteRepository;
    }

    @Override
    public List<NoteDto> getAllNotes() {
        List<Note> notes = this.noteRepository.findAll();
        List<NoteDto> noteDtos = notes.stream()
                .map((note) -> (NoteMapper.mapToNoteDto(note)))
                .collect(Collectors.toList());        
        return noteDtos;
    }

    @Override
    public void createNote(NoteDto noteDto) {
        Note note = NoteMapper.mapToNote(noteDto);
        noteRepository.save(note);
    }

    @Override
    public void updateNote(NoteDto noteDto) {
        Note note = NoteMapper.mapToNote(noteDto);
        System.out.println(noteDto);
        noteRepository.save(note);
    }

    @Override
    public void deleteNote(Long noteId) {
        noteRepository.deleteById(noteId);
    }

    @Override
    public NoteDto getNoteById(Long id) {
        Note ntd = noteRepository.findById(id).get();
        return NoteMapper.mapToNoteDto(ntd);
    }
}
