package UAS_PBO_2KS5_Dina_222111992;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UAS_PBO_2KS5_Dina_222111992Application {
	public static void main(String[] args) {
		SpringApplication.run(UAS_PBO_2KS5_Dina_222111992Application.class, args);
	}
}