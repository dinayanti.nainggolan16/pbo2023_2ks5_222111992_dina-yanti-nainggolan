package UAS_PBO_2KS5_Dina_222111992.service;

import UAS_PBO_2KS5_Dina_222111992.dto.UserDto;
import UAS_PBO_2KS5_Dina_222111992.entity.User;
import java.util.List;

/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 */
public interface UserService {
    void saveUser(UserDto userDto);

    User findUserByEmail(String email);

    List<UserDto> findAllUsers();
}

