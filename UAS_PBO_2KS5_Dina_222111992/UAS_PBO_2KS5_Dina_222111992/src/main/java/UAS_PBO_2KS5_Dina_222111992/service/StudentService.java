package UAS_PBO_2KS5_Dina_222111992.service;

import UAS_PBO_2KS5_Dina_222111992.dto.StudentDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Student;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 * Interface Student, 
 * Service-service yang bisa dikonsumsi oleh client terhadap Entity Student.
 * 
 * Business Logic Layer
 */
public interface StudentService {
    
    List<StudentDto> ambilDaftarStudent();
    void perbaruiDataStudent(StudentDto studentDto);
    void hapusDataStudent(Long studentId);
    void simpanDataStudent(StudentDto studentDto);
    StudentDto cariStudentById(Long id); 
    
    public List<StudentDto> searchStudent(String keyword);
    
    //Pagination
    public Page<Student> findByKetContainingIgnoreCase(String keyword, Pageable pageable);
    public Page<Student> findByNameContainingIgnoreCase(String keyword, Pageable pageable);
    public Page<Student> findAll(Pageable pageable);
   
}
