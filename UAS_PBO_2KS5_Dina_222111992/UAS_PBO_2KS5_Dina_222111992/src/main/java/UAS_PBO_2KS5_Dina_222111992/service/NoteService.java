package UAS_PBO_2KS5_Dina_222111992.service;

import UAS_PBO_2KS5_Dina_222111992.dto.NoteDto;

import java.util.List;
/**
 *
 * @author DINA NAINGGOLAN
 */
public interface NoteService {
    
    List<NoteDto> getAllNotes();
    void createNote(NoteDto noteDto);
    void updateNote(NoteDto noteDto);
    void deleteNote(Long noteid);
    NoteDto getNoteById(Long id);
}
