package UAS_PBO_2KS5_Dina_222111992.controller;

import UAS_PBO_2KS5_Dina_222111992.dto.NoteDto;
import UAS_PBO_2KS5_Dina_222111992.service.NoteService;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
/**
 *
 * @author DINA NAINGGOLAN
 */

@Controller
public class NoteController {
     private NoteService noteService;
     public NoteController(NoteService noteService) {
        this.noteService = noteService;
    }
     
     //handler method, GET Request return model (dto) dan view (templates/*.html)
    @GetMapping("/admin/notes")
    public String notes(Model model) {
        List<NoteDto> noteDtos = this.noteService.getAllNotes();
        // tambah atribut "noteDtos" yg bisa/akan digunakan di view
        model.addAttribute("noteDtos", noteDtos);
        // thymeleaf view: "/templates/admin/note.html"
        return "admin/notes";
    } 
    
    //handler method, GET Request return model (dto) dan view (templates/*.html)
    @GetMapping("/notesUser")
    public String notesUser(Model model) {
        List<NoteDto> noteDtos = this.noteService.getAllNotes();
        // tambah atribut "studentDtos" yg bisa/akan digunakan di view
        model.addAttribute("noteDtos", noteDtos);
        // thymeleaf view: "/templates/notesUser.html"
        return "notesUser";
    }
    
    @GetMapping("/admin/notes/add")
    public String addNoteForm(Model model) {
        NoteDto noteDto = new NoteDto();
        // tambah atribut "noteDto" yg bisa/akan digunakan di form th:object
        model.addAttribute("noteDto", noteDto);
        // thymeleaf view: "/templates/admin/note.html"
        return "admin/note_add_form";
    } 
    
    @PostMapping("/admin/notes")
    public String addNote( @Valid NoteDto noteDto, 
            BindingResult result){        
        if(result.hasErrors()){
            //model.addAttribute("noteDto", noteDto);
            return "admin/note_add_form";
        }
        noteService.createNote(noteDto);
        return "redirect:/admin/notes";
    }
    
    @GetMapping("/admin/notes/{noteId}/update")
    public String updateNoteForm (@PathVariable("noteId") Long ntdId,
            Model model) {
        NoteDto ntdDto = noteService.getNoteById(ntdId);
        model.addAttribute("noteDto", ntdDto);
        return "admin/note_update_form";
    }
    
    @PostMapping("admin/notes/update") 
    public String updateNote (@Valid NoteDto ntdDto, 
            BindingResult result) {
        
        if (result.hasErrors()) {
            //System.out.println(result.getFieldError());
            return "admin/note_update_form";
        }
        //System.out.println(stdDto);
        noteService.updateNote(ntdDto);
        return "redirect:/admin/notes";
    }
    
    @GetMapping("/admin/notes/{noteId}/delete") 
    public String deleteNote (@PathVariable("noteId") Long ntdId) {
        //System.out.println(ntdId);
        noteService.deleteNote(ntdId);        
        return "redirect:/admin/notes";
    }
}
