package UAS_PBO_2KS5_Dina_222111992.controller;

import UAS_PBO_2KS5_Dina_222111992.dto.StudentDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Student;
import UAS_PBO_2KS5_Dina_222111992.mapper.StudentMapper;
import UAS_PBO_2KS5_Dina_222111992.service.StudentService;
import jakarta.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author Dina
 * 222111992
 * 2KS5
 * Controller Class khusus untuk menjalankan Service terkait Student
 */
@Controller
public class StudentController {
    private StudentService studentService;

    /**
     * Constructor StudentController dengan parameter objek StudentService (DI)
     * @param studentService
     */
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }    
    
    /**
     * handler method untuk request view index 
     * @return String halaman index.html 
     */
    @GetMapping("/")
    public String index () {
        return "index";
    }
    
    @GetMapping("/admin/students")
    public String students(Model model, @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue = "1") int page, 
            @RequestParam(defaultValue = "5") int size){
        List<Student> students;{
        List<StudentDto> studentDtos;
        
        if (keyword.isEmpty()){
            studentDtos = this.studentService.ambilDaftarStudent();
        } else{
            studentDtos = this.studentService.searchStudent(keyword);
        }
        
        //pagination
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Student> studentPage;
        
        if (keyword == null) {
            studentPage = studentService.findAll(pageable);
        } else if (keyword.equalsIgnoreCase("name")) {
            studentPage = studentService.findByNameContainingIgnoreCase(keyword, pageable);
            model.addAttribute("keyword", keyword);
        } else {
            studentPage = studentService.findByKetContainingIgnoreCase(keyword, pageable);
            model.addAttribute("keyword", keyword);
        }
            
        
        students = studentPage.getContent();
        studentDtos = students.stream()
                .map(student -> StudentMapper.mapToStudentDto(student))
                .collect(Collectors.toList());
        
        model.addAttribute("studentDtos", studentDtos);
        model.addAttribute("currentPage", studentPage.getNumber()+1);
        model.addAttribute("totalItems",studentPage.getTotalElements());
        model.addAttribute("totalPages",studentPage.getTotalPages());
        model.addAttribute("pageSize",size);
        
        List<Integer> pageSizes = Arrays.asList(5,10,15,20);
        model.addAttribute("pageSizes",pageSizes);
        
        if(studentDtos.isEmpty()) {
            model.addAttribute("notFound", true);
        }
        // thymeleaf view: "/templates.html"
        return "admin/students";
    }
    }
    
     @GetMapping("/studentsUser")
    public String studentsUser(Model model, @RequestParam(defaultValue="") String keyword,
            @RequestParam(defaultValue = "1") int page, 
            @RequestParam(defaultValue = "5") int size){
        List<Student> students;{
        List<StudentDto> studentDtos;
        
        if (keyword.isEmpty()){
            studentDtos = this.studentService.ambilDaftarStudent();
        } else{
            studentDtos = this.studentService.searchStudent(keyword);
        }
        
        //pagination
        Pageable pageable = PageRequest.of(page-1,size);
        Page<Student> studentPage;
        
       if (keyword == null) {
            studentPage = studentService.findAll(pageable);
        } else {
            studentPage = studentService.findByNameContainingIgnoreCase(keyword, pageable);
            model.addAttribute("keyword", keyword);
        }
        
        students = studentPage.getContent();
        studentDtos = students.stream()
                .map(student -> StudentMapper.mapToStudentDto(student))
                .collect(Collectors.toList());
        
        model.addAttribute("studentDtos", studentDtos);
        model.addAttribute("currentPage", studentPage.getNumber()+1);
        model.addAttribute("totalItems",studentPage.getTotalElements());
        model.addAttribute("totalPages",studentPage.getTotalPages());
        model.addAttribute("pageSize",size);
        
        List<Integer> pageSizes = Arrays.asList(5,10,15,20);
        model.addAttribute("pageSizes",pageSizes);
        
        if(studentDtos.isEmpty()) {
            model.addAttribute("notFound", true);
        }
        // thymeleaf view: "/templates.html"
        return "studentsUser";
    }
    }
    

    /**
     * handler method, GET Request untuk menampilkan view student_add_form
     * @param model untuk menampung objek StudentDto
     * @return page student_add_form.html
     */
    @GetMapping("/admin/students/add")
    public String addStudentForm(Model model) {
        StudentDto studentDto = new StudentDto();
        // tambah atribut "studentDto" yg bisa/akan digunakan di form th:object
        model.addAttribute("studentDto", studentDto);
        // thymeleaf view: "/templates/admin/students.html"
        return "admin/student_add_form";
    }    
    
    
    /**
     * Handler method to handle student_add_form submit Post request
     * Valid : untk memvalidasi atribut2x pada model studentDto
     * (Optional) ModelAttribute : anotasi yang mengikat method parameter/nilai return dari metode 
     * ke atribut model bernama ("studentDto").apakah masih perlu jika namanya sama ?? 
     * @param studentDto objek studentDto yang dikirim oleh form pada view student_add_form
     * @param result Spring objek yang berisi hasil validasi dan memuat kumpulan error (jika ada)
     * @return page student_add_form.html (gagal), students.html (jika sukses)
     */
    @PostMapping("/admin/students")
    public String addStudent( @Valid StudentDto studentDto, 
            BindingResult result){        
        if(result.hasErrors()){
            //model.addAttribute("studentDto", studentDto);
            return "admin/student_add_form";
        }
        studentService.simpanDataStudent(studentDto);
        return "redirect:/admin/students";
    }
    
    /**
     * Handler Method untuk menampilkan Form Update Student
     * @param stdId id dari Student
     * @param model objek model untuk menampung objek studentDto
     * @return page student_update_form.html
     */
    @GetMapping("/admin/students/{studentId}/update")
    public String updateStudentForm (@PathVariable("studentId") Long stdId,
            Model model) {
        StudentDto stdDto = studentService.cariStudentById(stdId);
        model.addAttribute("studentDto", stdDto);
        return "admin/student_update_form";
    }
    
    /**
     * Handler method untuk memproses hasil dari Form update student
     * @param stdDto objek StudentDto yang sudah divalidasi
     * @param result hasil validasi dari semua atribut yg ada di StudentDto
     * @return String form update jika gagal, list student jika berhasil
     */
    @PostMapping("admin/students/update") 
    public String updateStudent (@Valid StudentDto stdDto, 
            BindingResult result) {
        
        if (result.hasErrors()) {
            //System.out.println(result.getFieldError());
            return "admin/student_update_form";
        }
        //System.out.println(stdDto);
        studentService.perbaruiDataStudent(stdDto);
        return "redirect:/admin/students";
    }
    
    /**
     * Handler Method untuk memproses hapus objek Student
     * @param stdId id Student yang ingin dihapus
     * @return String page list Student 
     */
    @GetMapping("/admin/students/{studentId}/delete") 
    public String deleteStudent (@PathVariable("studentId") Long stdId) {
        //System.out.println(stdId);
        studentService.hapusDataStudent(stdId);        
        return "redirect:/admin/students";
    }
} 
