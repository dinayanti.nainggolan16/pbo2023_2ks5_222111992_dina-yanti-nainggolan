package UAS_PBO_2KS5_Dina_222111992.controller;

import UAS_PBO_2KS5_Dina_222111992.dto.Email;
import UAS_PBO_2KS5_Dina_222111992.impl.EmailService;
import jakarta.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

/**
 *
 * @author DINA NAINGGOLAN
 */
@Controller
public class EmailController {
    
    @Autowired
    private EmailService emailService;
    
    @GetMapping("/email")
    public String email(){
        return "email";
    }
    @PostMapping("/sendMail")
    public String sendMail(@ModelAttribute Email email, HttpSession session){
        
        emailService.sendMail(email);
        session.setAttribute("msg", "Email Berhasil di Kirim");
        
        return"redirect:/email";
    }
}
