package UAS_PBO_2KS5_Dina_222111992.entity;

import jakarta.persistence.*; 
// hibernate 
import lombok.Setter;
import lombok.Getter;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 * Class Entity Student mewakili tabel Student di database.
 * GetSet Methods,Constructor dengan parameter semua field, dan default Constructor
 * otomatis ditambahkan saat compile time berlangsung menggunakan library lombok. 
 * 
 * Data Access Layer
 * 
 */

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_student")
public class Student {
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(nullable = false)
    private String name;
    
    @Column(nullable = false)
    private String nim;
    
    private int jan;
    private int feb;
    private int mar;
    private int apr;
    private int mei;
    private int juni;
    private int kurang;
    private String ket;    
}
