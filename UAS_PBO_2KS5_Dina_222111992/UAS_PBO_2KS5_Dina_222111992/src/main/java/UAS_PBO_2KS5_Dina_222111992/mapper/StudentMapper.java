package UAS_PBO_2KS5_Dina_222111992.mapper;
import UAS_PBO_2KS5_Dina_222111992.dto.StudentDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Student;
/**
 * @author Dina
 * 222111992
 * 2KS5
 * utility class berisi method static untuk 
 * mapping Entity ke Dto dan sebaliknya untuk Student. 
 */
public class StudentMapper {    
    // map Student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student) {
        // Membuat dto dengan builder pattern (inject dari lombok)
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .name(student.getName())
                .nim(student.getNim())
                .jan(student.getJan())
                .feb(student.getFeb())
                .mar(student.getMar())
                .apr(student.getApr())
                .mei(student.getMei())
                .juni(student.getJuni())
                .kurang(student.getKurang())
                .ket(student.getKet())
                .build();        
        return studentDto;
    }    
    // map Student Dto ke Student Entity
    public static Student mapToStudent(StudentDto studentDto) {
        Student student = Student.builder()
                .id(studentDto.getId())
                .name(studentDto.getName())
                .nim(studentDto.getNim())
                .jan(studentDto.getJan())
                .feb(studentDto.getFeb())
                .mar(studentDto.getMar())
                .apr(studentDto.getApr())
                .mei(studentDto.getMei())
                .juni(studentDto.getJuni())
                .kurang(studentDto.getKurang())
                .ket(studentDto.getKet())
                .build();        
        return student;
    }
}
