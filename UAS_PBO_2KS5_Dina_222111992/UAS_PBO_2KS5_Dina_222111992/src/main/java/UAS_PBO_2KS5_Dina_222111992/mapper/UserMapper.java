package UAS_PBO_2KS5_Dina_222111992.mapper;

import UAS_PBO_2KS5_Dina_222111992.dto.UserDto;
import UAS_PBO_2KS5_Dina_222111992.entity.User;
/**
 *
 * @author Dina
 * 222111992
 * 2KS5
 */
public class UserMapper {
    // map User entity to User Dto
    public static UserDto mapToUserDto(User user) {
        // Membuat dto dengan builder pattern (inject dari lombok)
        String[] str = user.getName().split(" ");
        UserDto userDto = UserDto.builder()
                .id(user.getId())
                .firstName(str[0])
                .lastName(str[1])
                .email(user.getEmail())
                .build();        
        return userDto;
    }    
    // map User Dto ke User Entity
    public static User mapToUser(UserDto userDto) {
        User user = User.builder()
                .id(userDto.getId())
                .name(userDto.getFirstName() + " " + userDto.getLastName())
                .email(userDto.getEmail())
                .build();        
        return user;
    }
}
