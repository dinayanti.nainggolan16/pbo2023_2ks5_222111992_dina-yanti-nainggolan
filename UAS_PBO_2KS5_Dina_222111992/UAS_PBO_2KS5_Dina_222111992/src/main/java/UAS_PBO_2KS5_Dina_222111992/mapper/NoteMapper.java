package UAS_PBO_2KS5_Dina_222111992.mapper;
import UAS_PBO_2KS5_Dina_222111992.dto.NoteDto;
import UAS_PBO_2KS5_Dina_222111992.entity.Note;
/**
 *
 * @author DINA NAINGGOLAN
 */

public class NoteMapper {
    // map Note entity to Note Dto
    public static NoteDto mapToNoteDto(Note note) {
        // Membuat dto dengan builder pattern (inject dari lombok)
        NoteDto noteDto = NoteDto.builder()
                .id(note.getId())
                .Judul(note.getJudul())
                .Isi(note.getIsi())
                .createdOn(note.getCreatedOn())
                .updatedOn(note.getUpdatedOn())
                .build();        
        return noteDto;
    }
    // map Note Dto ke Student Entity
    public static Note mapToNote(NoteDto noteDto) {
        Note note = Note.builder()
                .id(noteDto.getId())
                .Judul(noteDto.getJudul())
                .Isi(noteDto.getIsi())
                .createdOn(noteDto.getCreatedOn())
                .updatedOn(noteDto.getUpdatedOn())
                .build();        
        return note;
    }
}
