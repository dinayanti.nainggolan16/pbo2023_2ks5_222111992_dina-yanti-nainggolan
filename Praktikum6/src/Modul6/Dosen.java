package Modul6;

/**
 *
 * @author DINA NAINGGOLAN
 */
interface Dosen {

    public String getNIDN();
    public void setNIDN(String NIDN);
    public String getKeahlian();
    public String getPekerjaan();
    public void setKeahlian(String Keahlian);
}
