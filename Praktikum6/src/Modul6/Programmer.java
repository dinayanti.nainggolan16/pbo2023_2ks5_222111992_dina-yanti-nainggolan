package Modul6;
import java.util.Calendar;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Programmer extends Pegawai {
    private String bahasaPemrograman;
    private String platform;

    public Programmer(String nama, Calendar tanggalLahir) {
        super(nama, tanggalLahir);
    }
    
    public String getBahasa(){
       return bahasaPemrograman;
    }
    
    public void setBahasa(String Bahasa){
        this.bahasaPemrograman = Bahasa;
    }
    
    public String getPlatform(){
        return platform;
    }
    
    public String getPekerjaan(){
        return "Coding all along day";
    }
    
    public void setPlatform(String platform){
        this.platform = platform;
    }
}
