package Modul6;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author DINA NAINGGOLAN
 */
public abstract class Orang {
    private String nama;
    private Calendar tanggalLahir;

    public Orang(String nama, Calendar tanggalLahir) {
        this.nama = nama;
        this.tanggalLahir = tanggalLahir;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }
    
    public String getTanggalLahir() {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd");
            validateTanggalLahir();
            return sdf.format(tanggalLahir.getTime()).toString();
        } catch(Exception e){
            return "Belum tersedia.";
        } finally{
            System.out.println("Error tertangani.");
        }
     }
    
     void validateTanggalLahir() throws Exception{
        if(tanggalLahir.get(Calendar.YEAR)>2000){
            throw new Exception("Di bawah umur.");
        } else{
            System.out.println("Cukup umur.");
        }
    }
    
    public void setTanggalLahir(Calendar tanggalLahir) {
     this.tanggalLahir = tanggalLahir;
     }

    public String getNamaPanggilan() {
        return String.format("Nama Panggilan = %.4s ", nama);

    }

}
