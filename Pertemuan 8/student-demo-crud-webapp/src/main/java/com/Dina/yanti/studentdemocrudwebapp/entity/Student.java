package com.Dina.yanti.studentdemocrudwebapp.entity;

import jakarta.persistence.*;
//hibernate
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import java.util.Date;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
/**
 * Kelas student adalah kelas untuk menerima informasi atau representasi dari entitas siswa (students) dalam basis data
 * Kelas ini berisi atribut-atribut first Name, Last Name, Birthday yang merepresentasikan kolom-kolom dalam tabel siswa 
 * dan method-method yang berkaitan dengan entitas siswa, seperti getter dan setter.
 * 
 * @author DINA NAINGGOLAN
 */
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "tbl_student")

public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column (nullable = false)
    private String firstName;
    
    @Column (nullable = false)
    private String lastName;
    
    @Column (nullable = false)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    
    @Column (nullable = false)
    @CreationTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    
    @UpdateTimestamp
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn; 
}
