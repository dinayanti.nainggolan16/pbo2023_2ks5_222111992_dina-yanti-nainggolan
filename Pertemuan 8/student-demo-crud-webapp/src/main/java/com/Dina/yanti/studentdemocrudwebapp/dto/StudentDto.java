package com.Dina.yanti.studentdemocrudwebapp.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
/**
 * Kelas student dto memudahkan transfer data antara lapisan pengguna (user interface) 
 * dan lapisan bisnis atau lapisan akses data dalam aplikasi Spring Boot
 * Dengan menggunakan kelas StudentDTO, pengembang dapat memisahkan antara model data internal aplikasi 
 * dengan model data yang ditampilkan ke pengguna.
 * 
 * @author DINA NAINGGOLAN
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StudentDto {
    private Long id;
    
    @NotEmpty (message = "First Name should not be empty")
    private String firstName;
    
    @NotEmpty (message = "Last Name should not be empty")
    private String lastName;
    
    @NotNull (message = "Birth Date should not be empty")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthDate;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date createdOn;
    
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date updatedOn;
}
