package com.Dina.yanti.studentdemocrudwebapp.controller;

import com.Dina.yanti.studentdemocrudwebapp.dto.StudentDto;
import com.Dina.yanti.studentdemocrudwebapp.repository.StudentRepository;
import com.Dina.yanti.studentdemocrudwebapp.service.StudentService;
import com.Dina.yanti.studentdemocrudwebapp.service.StudentServiceImpl;
import jakarta.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

/**
 * Kelas controller merupakan kelas ntuk mengontrol aliran navigasi aplikasi web dengan menghubungkan permintaan HTTP ke metode yang sesuai di dalam aplikasi
 * Controller juga bertanggung jawab untuk memvalidasi input pengguna, mempersiapkan data untuk ditampilkan di tampilan, dan mengarahkan pengguna ke tampilan yang tepat
 * terdapat berbagai methode seperti GET, POST, PUT,maupun DELETE
 * dalam kelas ini akan banyak juga handler methode yang berfungsi menangani peristiwa tertentu dalam program
 * 
 * @author DINA NAINGGOLAN
 */

@Controller
public class StudentController {
    private StudentService studentService;
    public StudentController(StudentService studentService){
        this.studentService = studentService;
    }
    // handler method, GET Request return model (dto) dan view (templates/*.html)
    @GetMapping("/admin/students")
    public String students(Model model){
        List<StudentDto> studentDtos = this.studentService.ambilDaftarStudent();
        // tambah atribut "studentsDtos" yang bisa/akan digunakan di view
        model.addAttribute("studentDtos",studentDtos);
        // thymeleaf view: "/templates/admin/students.html"
        return "/admin/students";
    }
    // handler method untuk request view index
    @GetMapping("/")
    public String index(){
        return "index";
    }
    @GetMapping("admin/students/add")
    public String addStudentForm(Model model){
        StudentDto studentDto = new StudentDto();
        // tambah atribut "studentDto" yang bisa/akan digunakan di form th:object
        model.addAttribute("studentDto",studentDto);
        // thymeleaf view: "/template/admin/students.html"
        return "/admin/student_add_form";
    }
    @PostMapping("/admin/students/add")
    public String addStudent(@Valid StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_add_form";
        }
        studentService.simpanDataStudent(studentDto);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/delete/{id}")
    public String deleteStudent(@PathVariable("id") Long id){
        studentService.hapusDataStudent(id);
        return "redirect:/admin/students";
    }
    @GetMapping("/admin/students/update/{id}")
    public String updateStudentForm(@PathVariable("id") Long id, Model model){
        StudentDto studentDto = studentService.cariById(id);
        model.addAttribute("studentDto", studentDto);
        return "/admin/student_update_form";
    }
    @PostMapping("/admin/students/update")
    public String updateStudent(@Valid @ModelAttribute("studentDto") StudentDto studentDto, BindingResult result){
        if (result.hasErrors()){
            // model.addAttribute("studentDto", studentDto);
            return "/admin/student_update_form";
        }
        studentService.perbaruiDataStudent(studentDto);
        return "redirect:/admin/students";
    }
}