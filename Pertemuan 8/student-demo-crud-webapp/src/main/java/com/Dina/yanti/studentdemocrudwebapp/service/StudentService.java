package com.Dina.yanti.studentdemocrudwebapp.service;

import com.Dina.yanti.studentdemocrudwebapp.dto.StudentDto;
import com.Dina.yanti.studentdemocrudwebapp.entity.Student;
import java.util.List;
/**
 * Merupakan kelas yang akan di panggil dalam kelas controller
 * Kelas ini merupakan sebuah interface yang mendefinisikan layanan yang dibutuhkan 
 * oleh bagian lain dari aplikasi yang terkait dengan entitas siswa (students)
 * Interface ini berisi daftar metode yang berkaitan dengan operasi yang dapat dilakukan pada data siswa 
 * seperti menambah, mengubah, menghapus, dan mencari data siswa.
 * 
 * @author DINA NAINGGOLAN
 */

public interface StudentService {

    public List<StudentDto> ambilDaftarStudent();
    public void perbaruiDataStudent(StudentDto studentDto);
    public void hapusDataStudent(Long studentId);
    public void simpanDataStudent(StudentDto studentDto);

    public StudentDto cariById(Long id);
}
