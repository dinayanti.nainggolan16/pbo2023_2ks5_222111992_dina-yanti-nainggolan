package com.Dina.yanti.studentdemocrudwebapp.repository;

import com.Dina.yanti.studentdemocrudwebapp.entity.Student;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
/**
 * Kelas Repository bertanggung jawab untuk mengakses data dari sumber data seperti 
 * database, file, atau layanan web.
 * 
 * @author DINA NAINGGOLAN
 */

public interface StudentRepository extends JpaRepository<Student, Long>{
    // contoh method abstract baru.
    Optional<Student> findByLastName(String lastName);
    
}
