package com.Dina.yanti.studentdemocrudwebapp.service;

import com.Dina.yanti.studentdemocrudwebapp.dto.StudentDto;
import com.Dina.yanti.studentdemocrudwebapp.entity.Student;
import com.Dina.yanti.studentdemocrudwebapp.mapper.StudentMapper;
import com.Dina.yanti.studentdemocrudwebapp.repository.StudentRepository;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;
import java.util.Optional;
import org.springframework.stereotype.Service;
/**
 * Kelas StudentServiceImpl adalah implementasi dari interface StudentService yang bertanggung jawab 
 * untuk menyediakan layanan yang dibutuhkan oleh bagian lain dari aplikasi yang terkait dengan 
 * entitas siswa (students)
 * 
 * @author DINA NAINGGOLAN
 */

@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    
    public StudentServiceImpl(StudentRepository studentRepository){
        this.studentRepository = studentRepository;
    }
        
    @Override
    public List<StudentDto> ambilDaftarStudent(){
        List<Student> students = this.studentRepository.findAll();
        // konversi obj student ke studentDto satu per satu dengan fungsi MAP pada Array
        List<StudentDto> studentDtos = students.stream()
                .map(student -> (StudentMapper.mapToStudentDto(student)))
                .collect(Collectors.toList());
        return studentDtos;
    }
    @Override
    public void hapusDataStudent(Long studentId){
        this.studentRepository.deleteById(studentId);
    }
    @Override
    public void perbaruiDataStudent( StudentDto studentDto ){
        Student student = StudentMapper.mapToStudent(studentDto);
        this.studentRepository.save(student);
    }
    @Override
    public void simpanDataStudent(StudentDto studentDto){
        Student student = StudentMapper.mapToStudent(studentDto);
        studentRepository.save(student);
    }
    public StudentDto cariById(Long id){
        Student student = studentRepository.findById(id).orElse(null);
        StudentDto studentDto = StudentMapper.mapToStudentDto(student);
        return studentDto;
    }
}
