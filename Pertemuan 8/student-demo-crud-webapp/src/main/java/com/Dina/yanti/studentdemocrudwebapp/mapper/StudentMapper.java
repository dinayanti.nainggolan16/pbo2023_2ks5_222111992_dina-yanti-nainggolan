package com.Dina.yanti.studentdemocrudwebapp.mapper;

import com.Dina.yanti.studentdemocrudwebapp.dto.StudentDto;
import com.Dina.yanti.studentdemocrudwebapp.entity.Student;
/**
 * Fungsi utama dari kelas Mapper dalam Spring Boot adalah melakukan mapping antara objek di aplikasi dan data di dalam database menggunakan MyBatis
 * Dalam proses ini, Mapper melakukan beberapa tugas penting, antara lain
 * 1 Menentukan mapping antara nama kolom di database dan field di objek
 * 2 Melakukan query ke database untuk mengambil data dan mengembalikannya dalam bentuk objek
 * 3 Menyimpan data objek ke dalam database menggunakan query INSERT atau UPDATE
 * 4 Melakukan operasi CRUD (Create, Read, Update, dan Delete) pada data di database
 * 5 Melakukan kustomisasi query yang kompleks dengan menggunakan fitur-fitur MyBatis seperti dynamic SQL dan result mapping.
 * 
 * @author DINA NAINGGOLAN
 */

public class StudentMapper {
    //map student entity to Student Dto
    public static StudentDto mapToStudentDto(Student student){
        //Membuat dto dengan builder pattern (inject dari Lombok)
        StudentDto studentDto = StudentDto.builder()
                .id(student.getId())
                .firstName(student.getFirstName())
                .lastName(student.getLastName())
                .birthDate(student.getBirthDate())
                .createdOn(student.getCreatedOn())
                .updatedOn(student.getUpdatedOn())
                .build();
        return studentDto;
    }
    //map Student Dto ke Student Entity
    public static Student mapToStudent(StudentDto studentDto){
        Student student = Student.builder()
                .id(studentDto.getId())
                .firstName(studentDto.getFirstName())
                .lastName(studentDto.getLastName())
                .birthDate(studentDto.getBirthDate())
                .createdOn(studentDto.getCreatedOn())
                .updatedOn(studentDto.getUpdatedOn())
                .build();
        return student;
}
}
