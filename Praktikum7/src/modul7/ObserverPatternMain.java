package modul7;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class ObserverPatternMain {
    public static void main(String[] args){
        Pinkbook pinkbook = new Pinkbook(true);
        
        Customer customer1 = new Customer(pinkbook, "Luthfi");
        pinkbook.addObserver(customer1);
        
        Customer customer2 = new Customer(pinkbook, "Tuti");
        pinkbook.addObserver(customer2);
        
        pinkbook.setInStock(true);
    }
}

