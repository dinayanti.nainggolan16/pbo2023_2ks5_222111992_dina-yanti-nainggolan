package modul7;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class PegawaiKontrak extends Pegawai {
    public PegawaiKontrak (String nama){
        setNama(nama);
        setTipe("Kontrak");
        setPembayarangaji("Perjam");
    }
}

