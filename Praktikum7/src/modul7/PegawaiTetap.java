package modul7;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class PegawaiTetap extends Pegawai {
    public PegawaiTetap (String nama){
        setNama(nama);
        setTipe("Permanen");
        setPembayarangaji("Perbulan");
    }
}

