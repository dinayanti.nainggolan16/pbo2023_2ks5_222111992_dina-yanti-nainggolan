/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul2;

/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :Mahasiswamain5 untuk testing mahasiswa4
*/
public class MahasiswaMain5 {
    public static void main(String[] args) {
        Mahasiswa4 s1 = new Mahasiswa4(123456, "Luthfi");
        Mahasiswa4 s2 = new Mahasiswa4(123457, "Rahma");
        
        s1.tampilkanInfo();
        s2.tampilkanInfo();
    }
}
