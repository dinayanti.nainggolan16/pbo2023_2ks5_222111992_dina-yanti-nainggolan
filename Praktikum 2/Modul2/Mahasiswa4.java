/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modul2;

/*
* Author : 222111992_Dina
* Deskripsi Singkat Class :Mahasiswa4
*/
public class Mahasiswa4 {
    int nim;
    String nama;
    
    Mahasiswa4(int vnim, String vnama){
        nim = vnim;
        nama = vnama;
    }
    
    void tampilkanInfo(){
        System.out.println(nim+" "+nama);
    }
}
