/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package pertanian;
    
/**
 *
 * @author GHOLIDHO
 */
class Pertanian {
    private String nama_pemilik;
    String jenis_tanaman;
    protected int lebar;
    public int panjang;
    
    public Pertanian(String nama, String jenis, int lebar, int panjang){
        nama_pemilik = nama;
        jenis_tanaman = jenis;
        this.lebar = lebar;
        this.panjang = panjang;
    }
    
    public int getArea(){
        return panjang *lebar;
    }
    
    public String getNama(){
        return nama_pemilik;
    }
    
    public String jenis_tanaman(){
        return jenis_tanaman;
    }
    
    public String toString(){
        return "Pemilik: "+nama_pemilik+", Jenis Lahan: "+jenis_tanaman+", Panjang: "+panjang+", Lebar: "+lebar;
    }
}
    
class HasilPertanian extends Pertanian{
    private int hasil_panen;
    private int pengali;
    private int areaA;
    private int areaB;

    public HasilPertanian(String nama, String jenis, int lebar, int panjang) {
        super(nama, jenis, lebar, panjang);
        pengali = 2;
        areaA = lebar;
        areaB = panjang;
    }
    
    public int hasilPanen(){
        return hasil_panen = areaA*areaB*pengali;
    }
    
    @Override
    public String toString(){
        return "Hasil Panen: "+hasil_panen;
    }
}

class Pupuk extends Pertanian{
    private int jumlah_pupuk;
     private int areaA;
    private int areaB;

    public Pupuk(String nama, String jenis, int lebar, int panjang) {
        super(nama, jenis, lebar, panjang);
        areaA = lebar;
        areaB = panjang;
    }
    
    public int jumlahPupuk(){
        return jumlah_pupuk = areaA*areaB/2;
    }
    
    @Override
    public String toString(){
        return "Jumlah Pupuk: "+jumlah_pupuk;
    }
}

public class TestPertanian{
    public static void main(String[] args) {
        Pertanian p1 = new Pertanian("Iqbal", "Padi", 15, 30);
        System.out.println(p1);
        
        HasilPertanian p2 = new HasilPertanian("Iqbal", "Padi", 15, 30);
        p2.hasilPanen();
        System.out.println(p2);
        
        Pupuk p3 = new Pupuk("Iqbal", "Padi", 15, 30);
        p3.jumlahPupuk();
        System.out.println(p3);
        System.out.println(p3.getArea());
    }
} 