/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package custom;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Salah extends Exception{ //meng-Extends Exception dari java
    private String eksepsi;
    
    public Salah(){
        super();
        this.eksepsi = " ";
    }
    
    public Salah(String e){
        super();
        this.eksepsi = e;
    }
    
    @Override
    public String getMessage(){
        return this.eksepsi;
    }
}
