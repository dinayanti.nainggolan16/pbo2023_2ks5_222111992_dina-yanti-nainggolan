package custom;

/**
 *
 * @author DINA NAINGGOLAN
 */
public class Main {
    public void validasi(String n) throws Salah{
        if (n.equals("Dina")){
            System.out.println("Mahasiswi"); // hasil keluaran jika nama sama
        } else{
            throw new Salah ("Salah Tulis Nama, seharusnya bukan " + n);// hasil keluaran jika nama tidak sama
        }
    }
    
    public static void main(String args[]){
        Main m = new Main();
        try{
            m.validasi("Fani"); //nama tidak sama
        } catch(Salah e){
            System.out.println(e.getMessage()); //akan mengeluarkan massage "Salah Tulis"
        }
    }
}
